﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_10
{
    class Program
    {
        // select - (Опреация проекции) используется для производства конечного результата запроса.
        static void Main(string[] args)
        {
            int[] numbers = { 1, 2, 3, 4, 5, 6, 7 };
            // Построить запрос.
            var query = from n in numbers
                        select new
                        {
                            Input = n,
                            Output = n * 2
                        };
            foreach (var item in query)
            {
                Console.WriteLine($"Input: {item.Input} Output: {item.Output}");
            }



            Console.ReadLine();
        }
    }
}
