﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dynamic_02
{
    class Program
    {
        static dynamic field = 1;
        // Динамические типы данных. (Статические поля)

        static void Main(string[] args)
        {
            Console.WriteLine(field);

            field = "Hello world!";

            Console.WriteLine(field);

            field = DateTime.Now;

            Console.WriteLine(field);



            Console.ReadLine();
        }
    }
}
