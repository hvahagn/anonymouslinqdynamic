﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_08
{
    // orderby - используется для сортировки (последовательности) результата запроса.
    public class Employee
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Nationality { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {

            // Построить коллекцию сотрудников с национальностями.
            var employees = new List<Employee>
            {
                new Employee {LastName = "Ivanov", FirstName = "Ivan", Nationality = "Russian"},
                new Employee {LastName = "Hovhannisyan", FirstName = "Andranik", Nationality = "Armenian"},
                new Employee {LastName = "Petrov", FirstName = "Petr", Nationality = "American"}
            };

            // Построить запрос.
            // Получение списка имен всех сотрудников вместе с их национальностями.
            var query = from emp in employees
                        orderby emp.Nationality ascending, emp.LastName descending, emp.FirstName descending
                        select emp;

            foreach (var person in query)
            {
                Console.WriteLine($"{person.LastName} {person.FirstName} {person.Nationality}");
            }




            Console.ReadLine();
        }
    }
}
