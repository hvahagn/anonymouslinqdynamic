﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dynamic_03
{
    class Program
    {
        dynamic field = 1, field2 = "Hello", field3 = true;
        // Динамические типы данных. (Нестатические поля)
        static void Main(string[] args)
        {
            dynamic instance = new Program();

            Console.WriteLine(instance.field);

            instance.field = "Hello world!";

            Console.WriteLine(instance.field);

            instance.field = DateTime.Now;

            Console.WriteLine(instance.field);


            Console.ReadLine();
        }
    }
}
