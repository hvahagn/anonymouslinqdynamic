﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dynamic_01
{
    class Program
    {
        static void Main(string[] args)
        {
            dynamic variable = 1;
            Console.WriteLine(variable);
            Console.WriteLine(variable.GetType());

            variable = "Hello world";
            Console.WriteLine(variable);
            Console.WriteLine(variable.GetType());

            variable = DateTime.Now;
            Console.WriteLine(variable);
            Console.WriteLine(variable.GetType());



            Console.ReadLine();
        }
    }
}
