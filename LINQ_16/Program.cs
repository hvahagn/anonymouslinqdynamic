﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_16
{
    class Program
    {
        // into - подобно let, позволяет определить локальный по отношению к запросу идентификатор.
        static void Main(string[] args)
        {
            int[] numbers = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            // Построить запрос.
            // Разделение чисел на четные и нечетные.
            var query = from x in numbers
                        group x by x % 2 into partition
                        select new { Key = partition.Key, Count = partition.Count(), Group = partition };

            foreach (var item in query)
            {
                Console.WriteLine("Mod2={0}", item.Key);
                Console.WriteLine("Count ={0}", item.Count);

                foreach (var number in item.Group)
                {
                    Console.WriteLine("{0} ", number);
                }
                Console.WriteLine();

            }

            Console.ReadLine();
        }
    }
}
