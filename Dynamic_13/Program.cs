﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dynamic_13
{
    // Динамические типы данных. (Анонимные типы)
    class Program
    {
        static void Main(string[] args)
        {
            dynamic instance = new { Name = "Alex", Age = 18 };

            Console.WriteLine(instance.Name);
            Console.WriteLine(instance.Age);

            // Delay.
            Console.ReadKey();
        }
    }
}
