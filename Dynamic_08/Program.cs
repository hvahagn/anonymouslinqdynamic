﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dynamic_08
{
    // Динамические типы данных. (Динамические типы в параметризированных делегатах)
    class Program
    {
        delegate R MyDelegate<T, R>(T argument);

        
            static dynamic Method(dynamic argument)
            {
                return argument;
            }

            static void Main()
            {
                dynamic myDelegate = new MyDelegate<dynamic, dynamic>(Method);

                dynamic @string = myDelegate("Hello world!");

                Console.WriteLine(@string);

                // Delay.
                Console.ReadKey();
            }
        }
    }

