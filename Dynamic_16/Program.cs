﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dynamic_16
{
    class Calculator
    {
        public dynamic Add(dynamic a, dynamic b)
        {
            return a + b;
        }
    }

    class Program
    {
        static void Main()
        {
            dynamic calculator = new Calculator();

            Console.WriteLine(calculator.Add(2, 3));
            Console.WriteLine(calculator.Add("1", 2));
            Console.WriteLine(calculator.Add(2.55, 2));


            // Delay.
            Console.ReadKey();
        }
    }

}
