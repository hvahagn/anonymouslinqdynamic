﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dynamic_05
{
    class Program
    {
        // Динамические типы данных. (Динамические типы аргументов и возвращаемых значений методов.)
        static dynamic Method(dynamic argument)
        {
            return "Hello " + argument + "!";
        }
        static void Main(string[] args)
        {
           string vh= Method("Vahagn");
            Console.WriteLine(vh);

            string vh2 = Method(101);
            Console.WriteLine(vh2);


            Console.ReadLine();
        }
    }
}
