﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace LINQ_06
{
    // Применение запроса к коллекции которая поддерживает IEnumerable не параметризированный.
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList numbers = new ArrayList();
            numbers.Add(1);
            numbers.Add(2);
            // ЯВНОЕ указание типа Int32 переменной диапазона - n.  (var - НЕВОЗМОЖНО использовать т.к. IEnumerable не параметризированный!)
            var query = from int n in numbers
                        select n * 2;

            foreach (var item in query)
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }
    }
}
