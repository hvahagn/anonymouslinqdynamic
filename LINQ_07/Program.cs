﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_07
{
    // join (операция объединения) - устанавливает соотношение данных из двух разных источников.
    public class EmployeeID
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class EmployeeNationality
    {
        public string Id { get; set; }
        public string Nationality { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var employees = new List<EmployeeID>
            {
                new EmployeeID{Id="111", Name="Ivan Ivanov"},
                new EmployeeID {Id = "222", Name = "Andrey Andreev"},
                new EmployeeID {Id = "333", Name = "Petr Petrov"},
                new EmployeeID {Id = "444", Name = "Alex Alexeev"}


             };
            var empNationalities = new List<EmployeeNationality>
            {
                new EmployeeNationality{Id="111", Nationality="Armenian"},
                new EmployeeNationality {Id = "211", Nationality = "Russian"},
                new EmployeeNationality {Id = "222", Nationality = "Ukrainian"},
                new EmployeeNationality {Id = "333", Nationality = "American"},

            };

            // Построить запрос.
            // Получение списка имен всех сотрудников вместе с их национальностями, при этом отсортировав список по убыванию.
            

            var query = from emp in employees
                        join n in empNationalities
                        on emp.Id equals n.Id
                        orderby n.Nationality descending// ascending - по возрастанию | descending - по убыванию.
                        select new
                        {
                            Id = emp.Id,
                            Name = emp.Name,
                            Nationality = n.Nationality
                        };

            foreach (var item in query)
            {
                Console.WriteLine(item.Id+"\t "+item.Name+"\t "+item.Nationality);
            }






            Console.ReadLine();
        }
    }
}
