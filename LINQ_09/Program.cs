﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_09
{
    // select - (Операция проекции) используется для производства конечного результата запроса.
    public class Result
    {
        public Result(int input, int output)
        {
            Input = input;
            Output = output;
        }

        public int Input { get; set; }
        public int Output { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {

            int[] numbers = { 1, 2, 3, 4, 5, 6, 7, 8 };
            // Построить запрос.
            var query = from n in numbers
                        select new Result(n, n * 2);
            Console.WriteLine("Выражение запроса выполняется в момент обращения к переменной запрса в foreach");

            numbers[0] = 777; // Выражение запроса выполняется в момент обращения к переменной запрса в foreach

            foreach (var item in query)
            {
                Console.WriteLine($"Input: {item.Input} Output: {item.Output}");
            }

            Console.ReadLine();
        }
    }
}
