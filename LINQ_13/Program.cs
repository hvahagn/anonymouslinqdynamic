﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_13
{
    class Program
    {
        // group - является средством для разделения ввода запроса.
        static void Main(string[] args)
        {
            int[] numbers = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            // Построить запрос.
            // Разделение чисел на четные и нечетные.
            var query = from n in numbers
                        group n by n % 3;

          
            foreach (var group in query)
            {
                Console.WriteLine("Mod {0}=={0}", group.Key);
                foreach (var number in group)
                {
                    Console.WriteLine("{0} ", number);

                }
            }




            Console.ReadLine();
        }
    }
}
