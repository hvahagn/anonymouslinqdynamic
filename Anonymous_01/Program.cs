﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous_01
{
    class MyClass
    {
        public string field;
        public void Method()
        {
            Console.WriteLine(field);
        }
    }
    delegate void MyDelegate(string @string);
    class Program
    {
        static void Main(string[] args)
        {
            //1
            var instance = new { Name = "Alex", Age = 27, Id = new { Number = 101 } };
            Console.WriteLine(instance.Name + " " + instance.Age + " " + instance.Id.Number);
            //2
            var instance2 = new { My = new MyClass() };
            instance2.My.field = "Hello";
            instance2.My.Method();
            //3
            new
            {
                My = new MyClass
                {
                    field = "World"
                }
            }.My.Method();

            //4
            var instance3 = new { My = new MyDelegate((string @string)=>Console.WriteLine(@string)) 
        };

            instance3.My("Hello world");



            Console.ReadLine();
        }
    }
}
